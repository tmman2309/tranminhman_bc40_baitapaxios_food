// RenderFoodList
function renderFoodList(foodList) {
  var contentHTML = "";

  foodList.reverse().forEach(function (item) {
    var contentTr = `
                    <tr>
                        <td>${item.maMon}</td>
                        <td>${item.tenMon}</td>
                        <td>${item.giaMon}</td>
                        <td>${
                          item.loaiMon == true
                            ? "<span class='text-danger'>Mặn</span>"
                            : "<span class='text-primary'>Chay</span>"
                        }</td>
                        <td>${convertString(30, item.hinhAnh)}</td>
                        <td>
                            <button onclick="xoaMonAn('${
                              item.maMon
                            }')" class="btn btn-danger">Xóa</button>
                            <button onclick="suaMonAn('${
                              item.maMon
                            }')" class="btn btn-warning">Sửa</button>
                        </td>
                    </tr>
        `;
    contentHTML += contentTr;
  });

  return (document.getElementById("foodListBody").innerHTML = contentHTML);
}

// Lấy thông tin từ form
function layThongTinTuForm() {
  var maMon = document.getElementById("maMon").value;
  var tenMon = document.getElementById("tenMon").value;
  var giaMon = document.getElementById("giaMon").value;
  var hinhAnh = document.getElementById("hinhAnh").value;
  var loaiMon = document.getElementById("loaiMon").value;

  var monAn = {
    maMon: maMon,
    tenMon: tenMon,
    giaMon: giaMon,
    hinhAnh: hinhAnh,
    loaiMon: loaiMon == "man" ? true : false,
  };

  return monAn;
}

// Show thông tin từ object
function showThongTinObject(monAn) {
  document.getElementById("maMon").value = monAn.maMon;
  document.getElementById("tenMon").value = monAn.tenMon;
  document.getElementById("giaMon").value = monAn.giaMon;
  document.getElementById("hinhAnh").value = monAn.hinhAnh;
  console.log(monAn.loaiMon)
  if(monAn.loaiMon){
    document.getElementById("loaiMon").value = "man";
  } else{
    document.getElementById("loaiMon").value = "chay";
  }
}

// ConvertString
function convertString(maxLength, item) {
  if (item.length > maxLength) {
    return item.slice(0, maxLength) + "...";
  } else {
    return item;
  }
}

// Bật & Tắt Loading
function batLoading() {
  document.getElementById("spinner").style.display = "flex";
}

function tatLoading() {
  document.getElementById("spinner").style.display = "none";
}

// Bật & Tắt Button
function batThemMon(){
  document.getElementById("themMon").style.display = "block";
}

function tatThemMon(){
  document.getElementById("themMon").style.display = "none";
}

function batCapNhatMon(){
  document.getElementById("capNhatMon").style.display = "block";
}

function tatCapNhatMon(){
  document.getElementById("capNhatMon").style.display = "none";
}

// Bật & Tắt Mã Món
function tatMaMon(){
  document.getElementById("maMon").setAttribute("disabled","");
}

function batMaMon(){
  document.getElementById("maMon").removeAttribute("disabled","");
}
