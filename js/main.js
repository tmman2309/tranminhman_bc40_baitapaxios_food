const BASE_URL = "https://63b53b1c0f49ecf5089e1b45.mockapi.io";

// Gọi API Food
function fetchFoodList() {
  batLoading();
  axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  })
    .then(function (res) {
      renderFoodList(res.data);
      tatLoading();
    })
    .catch(function (err) {
      console.log("🚀 ~ file: main.js:12 ~ fetchFoodList ~ err", err);
      tatLoading();
      batMaMon();
    });
}
// Khi users load lại trang
tatCapNhatMon();
batThemMon();
fetchFoodList();

// Thêm món ăn
function themMonAn() {
  batLoading();
  var monAn = layThongTinTuForm();

  axios({
    url: `${BASE_URL}/food`,
    method: "POST",
    data: monAn,
  })
    .then(function (res) {
      tatLoading();
      fetchFoodList();
    })
    .catch(function (err) {
      tatLoading();
      console.log("🚀 ~ file: main.js:37 ~ themMonAn ~ err", err);
    });
}

// Xóa món ăn
function xoaMonAn(maMon) {
  batLoading();
  axios({
    url: `${BASE_URL}/food/${maMon}`,
    method: "DELETE",
  })
    .then(function (res) {
      tatLoading();
      fetchFoodList();
    })
    .catch(function (err) {
      tatLoading();
      console.log("🚀 ~ file: main.js:28 ~ xoaMonAn ~ err", err);
    });
}

// Cập nhật món ăn
function suaMonAn(maMon) {
  axios({
    url: `${BASE_URL}/food/${maMon}`,
    method: "GET",
  })
    .then(function (res) {
      tatThemMon();
      batCapNhatMon();
      tatMaMon();
      showThongTinObject(res.data);
    })
    .catch(function (err) {
      tatThemMon();
      batCapNhatMon();
      tatMaMon();
      console.log("🚀 ~ file: main.js:69 ~ suaMonAn ~ err", err);
    });
}

function capNhatMonAn(){
  batLoading();
  var monAn = layThongTinTuForm();

  axios({
    url: `${BASE_URL}/food/${monAn.maMon}`,
    method: "PUT",
    data: monAn,
  })
  .then(function(res){
    tatLoading();
    fetchFoodList();
  })
  .catch(function(err){
    tatLoading();
    console.log("🚀 ~ file: main.js:93 ~ capNhatMonAn ~ err", err)
  })
}
